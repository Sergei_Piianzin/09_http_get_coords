import re

#Gets following string: InternalToponymInfo *** coordinates : ***
prog = re.compile('InternalToponymInfo.*?\[.*?\]')
pairs=""
with open("data.json", encoding='utf-8') as f:
    for line in f:
        match = re.search(prog, line)
        if (match) :
        	pairs = match.group(0)
        	break

#Gets pair of coordinates 
prog = re.compile('(?<=coordinates\":)\[.*?\]')  
match = re.search(prog, pairs)		
print(match.group(0))