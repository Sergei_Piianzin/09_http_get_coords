import re

#Gets following string: csrfToken":"**...*"
prog = re.compile('csrfToken\":\".[^\"]*')
pair=""
with open("response", encoding='utf-8') as f:
    for line in f:
        match = re.search(prog, line)
        if (match) :
        	pair = match.group(0)
        	break	
#Gets token: 
prog = re.compile('(?<=csrfToken\":\").*[^\""]?')
csrfToken = re.search(prog, pair)
print(csrfToken.group(0))